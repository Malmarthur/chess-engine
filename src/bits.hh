#pragma once

#include <cassert>
#include <cstdint>
#include <iostream>

namespace bits
{
    inline int get_lsb_index(uint64_t &bitboard)
    {
        assert(bitboard != 0);
        return __builtin_ctzll(bitboard);
    }

    inline int pop_lsb(uint64_t &bitboard)
    {
        assert(bitboard != 0);
        // Get number of trailing 0 bits
        int index = get_lsb_index(bitboard);
        // "Pop" the last '1'
        bitboard &= ~(1ULL << index);
        return index;
    }
    inline int count_bit(uint64_t bitboard)
    {
        bitboard = bitboard - ((bitboard >> 1) & 0x5555555555555555);
        bitboard = (bitboard & 0x3333333333333333)
                   + ((bitboard >> 2) & 0x3333333333333333);
        return (((bitboard + (bitboard >> 4)) & 0xF0F0F0F0F0F0F0F)
                * 0x101010101010101)
                >> 56;
    }
    inline void set_bit(uint64_t &number, int bit)
    {
        number |= 1ULL << bit;
    }

    inline void unset_bit(uint64_t &number, int bit)
    {
        number &= ~(1ULL << bit);
    }

    inline bool bit_is_set(uint64_t number, int bit)
    {
        return ((number >> bit) & 1ULL);
    }

    inline void print_bitboard(uint64_t board)
    {
        std::cout << "======================" << std::endl;
        for (int y = 7; y >= 0; y--)
        {
            for (int x = 0; x < 8; x++)
            {
                if (bits::bit_is_set(board, y * 8 + x))
                    std::cout << "1  ";
                else
                    std::cout << ".  ";
            }
            std::cout << std::endl;
        }
        std::cout << "======================" << std::endl;
    }
} // namespace bits