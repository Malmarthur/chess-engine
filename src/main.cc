#include <iostream>
#include "board.hh"

int main(int argc, char *argv[]) {
    Board board = Board();
    std::cout << "Board initialized:\n";
    board.pretty_print();
}