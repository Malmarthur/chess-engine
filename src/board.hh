#pragma once

#include <bitset>

class Board {
public:
    Board();

    uint64_t bitboards[12]{};
    void pretty_print();
};