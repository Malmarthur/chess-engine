#include "board.hh"
#include "bits.hh"


Board::Board() {
    // **** WHITE ****
    bitboards[0] = 0x10; // KING
    bitboards[1] = 0x8; // QUEEN
    bitboards[2] = 0x81; // ROOKS
    bitboards[3] = 0x24; // BISHOPS
    bitboards[4] = 0x42; // KNIGHTS
    bitboards[5] = 0xFF00; // PAWNS

    // **** BLACK ****
    bitboards[6] = 0x1000000000000000; // KING
    bitboards[7] = 0x800000000000000; // QUEEN
    bitboards[8] = 0x8100000000000000; // ROOKS
    bitboards[9] = 0x2400000000000000; // BISHOPS
    bitboards[10] = 0x4200000000000000; // KNIGHTS
    bitboards[11] = 0x00FF000000000000; // PAWNS
}

void Board::pretty_print() {
    std::string to_char[] = { "♔", "♕", "♖", "♗", "♘", "♙",
                              "♚", "♛", "♜", "♝", "♞", "♟"};

    for (int y = 7; y >= 0; y--) {
        for (int x = 7; x >= 0; x--) {
            bool piece_present = false;
            for (int i = 0; i < 12; i++) {
                if (bits::bit_is_set(bitboards[i], y * 8 + x)) {
                    piece_present = true;
                    std::cout << to_char[i];
                }
            }

            if (!piece_present)
                std::cout << ".";
            std::cout << "  ";
        }

        std::cout << "\n";
    }
}